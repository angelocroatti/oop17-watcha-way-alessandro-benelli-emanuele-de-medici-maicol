# OOP17 - Alessandro Benelli, Emanuele De Medici, Maicol Mancini - Watch Your Way


All�avvio dell�applicazione, il giocatore si trover� davanti al menu iniziale tramite il quale potr� iniziare il gioco oppure accedere alle impostazioni per regolare il volume.


Una volta iniziato il gioco, apparir� un breve tutorial per spiegare il funzionamento base delle meccaniche che sono elencate di seguito:

- I tasti W, A, S, D (� stato implementato anche il supporto alle frecce direzionali) permettono al giocatore di muoversi all�interno delle mappe dei livelli.

- Il tasto ESC avvia il menu di pausa che sospende la partita e permette di riprenderlo in ogni momento, visualizzare le impostazioni o ritornare al menu iniziale.

- Per completare un livello, il giocatore che parte in un punto prestabilito, dovr� calpestare tutte le caselle presenti senza, per�, mai passare due volte sulla stessa e raggiungere il traguardo. Completare con successo un livello permette al giocatore di raggiungere il successivo.

- Calpestare due volte la stessa casella oppure raggiungere il traguardo senza completare correttamente il percorso comporter� il game-over e il giocatore sar� costretto a ricominciare il gioco dal primo livello.

- Il gioco sar� considerato completato quando tutti i cinque livelli saranno risolti correttamente. A gioco finito, il giocatore visualizzer� il tempo impiegato e la sua posizione nella classifica personale. A questo punto potr� scegliere se provare a battere s� stesso oppure chiudere l�applicativo.


Il nostro record � di 32.09s, riuscirete a fare di meglio?