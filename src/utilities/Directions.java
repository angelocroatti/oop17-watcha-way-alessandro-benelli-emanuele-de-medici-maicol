package utilities;

/**
 * Enumeration of directions.
 *
 */
public enum Directions {

    /**
     * Up direction.
     */
    UP,

    /**
     * Down direction.
     */
    DOWN,

    /**
     * Left direction.
     */
    LEFT,

    /**
     * Right direction.
     */
    RIGHT;
}
